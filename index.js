const AWS = require("aws-sdk");
const redis = require("redis");
const db = new AWS.DynamoDB.DocumentClient();

const production = process.env.PRODUCTION === 'true'

const tableName = "image_sharing";

const clientRedis = redis.createClient({
  host: production ? '54.157.78.122' : 'localhost',
  port: 6379,
  password: 's3cur3p4ssw0rd'
});

const search = async (keyword, categories = '', sortByDownload = false, next) => {
  const key = keyword + ":" + categories + ":" + (sortByDownload ? 't' : 'f');
  return clientRedis.GET(key, (err, reply) => {
    if (err) {
      next(err);
    }
    if (reply == null) {
      const params = {
        ExpressionAttributeValues: {
          ':categories': { S: categories },
          ':keyword': { S: keyword },
        },
        ExpressionAttributeNames: {
          '#categories': "category",
          '#name': "name",
        },
        TableName: tableName,
        FilterExpression: 'contains(#categories, :categories) AND contains(#name, :keyword)',
        ScanIndexForward: sortByDownload ? true : false
      }
      return db.scan(params, (err, data) => {
        if (err) {
          next(err);
        }
        if (data == null) {
          return next(null, { Items: [] });
        }
        else {
          const toCache = JSON.stringify(data);
          clientRedis.SET(key, toCache);
          return next(null, data);
        }
      })
    } else {
      return next(null, JSON.parse(reply));
    }
  })
}

exports.handler = async (event, context, next) => {
  const query = event.queryStringParameters;
  const { keyword, categories, sortByDownload } = query;
  search(keyword, categories, sortByDownload === 'true', (err, data) => {
    next(null, {
      statusCode: err ? 500 : 200, 
      headers: {
          "Content-Type": "application/json"
      },
      body: JSON.stringify(err || data),
    })
  });
};
