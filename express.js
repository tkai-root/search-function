const express = require('express')
const bodyParser = require('body-parser')
const { handler } = require('./index')

const app = express()

const PORT = process.env.PORT || 3000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', async (req, res) => {
  handler({ queryStringParameters: req.query }, null, (err, data) => {
    res.send(data)
  });
});

app.listen(PORT, () => {
  console.log('Server connected at:', PORT)
})
